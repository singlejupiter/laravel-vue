<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/currencies', 'App\Http\Controllers\CurrencyController@index');
    Route::get('/currencies/search', 'App\Http\Controllers\CurrencyController@search');
    Route::get('/currency/{currency}', 'App\Http\Controllers\CurrencyController@detail');
    Route::get('/currency/{currency}/history', 'App\Http\Controllers\CurrencyController@history');
});


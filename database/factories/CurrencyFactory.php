<?php

namespace Database\Factories;

use App\Models\Currency;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CurrencyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Currency::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'num_code' => $this->faker->unique()->numberBetween(1, 10000),
            'char_code' => Str::random(3),
            'nominal' => $this->faker->numberBetween(1, 10000),
            'name' => $this->faker->name,
        ];
    }
}

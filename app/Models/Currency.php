<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        'num_code',
        'char_code',
        'nominal',
        'name',
    ];

    public function rates()
    {
        return $this->hasMany(CurrencyRate::class);
    }

    public function dailyRates()
    {
        $rates = $this->rates()->get();
        $daily_rates = [];
        foreach ($rates as $rate) {
            $daily_rates[$rate->date] = $rate->rate;
        }
        return $daily_rates;
    }
}

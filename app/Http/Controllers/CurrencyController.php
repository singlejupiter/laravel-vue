<?php

namespace App\Http\Controllers;

use App\Http\Resources\CurrencyResource;
use App\Models\Currency;
use App\Models\CurrencyRate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CurrencyController extends Controller
{
    /**
     * Return list of currencies.
     * @return Currency[]
     */
    public function index()
    {
        return Currency::all()->toArray();
    }

    /**
     * Search currencies.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        $page = $request['page'] ? $request['page'] - 1 : 0;
        $page_size = $request['page_size'];
        $date = $request['date'];
        $base_currency_id = $request['base_currency_id'];

        if (!$date)
            $date = CurrencyRate::max('date');

        if (isset($base_currency_id)) {
            $currencies = Currency::query()
                ->when($page_size, function ($query) use ($page, $page_size) {
                    return $query->offset($page * $page_size)->limit($page_size);
                })
                ->get();

            $base_currency = Currency::find($base_currency_id);
            if (!$base_currency) {
                return response('Cannot find base currency', 404);
            }
            $base_currency_rates = $base_currency->dailyRates();

            foreach ($currencies as $currency) {
                $daily_rates = $currency->dailyRates();
                $rates = [];
                $current_rate = null;
                foreach ($daily_rates as $rate_date => $rate) {
                    if ($base_currency_rates[$rate_date]) {
                        $rate /= $base_currency_rates[$rate_date];
                        $rates []= $rate;
                        if ($date == $rate_date)
                            $current_rate = $rate;
                    }
                }

                $currency->rate = $current_rate;
                $currency->rates_max_rate = max($rates);
                $currency->rates_min_rate = min($rates);
                $currency->rates_avg_rate = array_sum($rates) / count($rates);
            }
        } else {
            $currencies = Currency::query()
                ->leftJoin('currency_rates', function ($join) use ($date) {
                    $join->on('currency_rates.currency_id', '=', 'currencies.id')
                        ->where('currency_rates.date', $date);
                })
                ->selectRaw('currencies.*, currency_rates.rate')
                ->withMax('rates', 'rate')
                ->withMin('rates', 'rate')
                ->withAvg('rates', 'rate')
                ->when($page_size, function ($query) use ($page, $page_size) {
                    return $query->offset($page * $page_size)->limit($page_size);
                })
                ->get();
        }
        $currencies = CurrencyResource::collection($currencies);

        return response([
            'date' => $date,
            'currencies' => $currencies,
        ]);
    }

    /**
     * Search currencies.
     * @param Currency $currency
     * @return Response
     */
    public function detail(Currency $currency)
    {
        if (!$currency) {
            return response('Cannot find currency', 404);
        }

        return response([
            'currency' => $currency,
            'history_count' => $currency->rates()->count(),
        ]);
    }

    /**
     * Search currencies.
     * @param Request $request
     * @param Currency $currency
     * @return Response
     */
    public function history(Request $request, Currency $currency)
    {
        if (!$currency) {
            return response('Cannot find currency', 404);
        }

        $page = $request['page'] ? $request['page'] - 1 : 0;
        $page_size = $request['page_size'];
        $date_from = $request['date_from'];
        $date_to = $request['date_to'];
        $base_currency_id = $request['base_currency_id'];

        $query = $currency->rates()
            ->when($date_from, function ($query, $date_from) {
                return $query->where('date', '>=', $date_from);
            })
            ->when($date_to, function ($query, $date_to) {
                return $query->where('date', '<=', $date_to);
            })
            ->select('date', 'rate');

        $total_count = $query->count();

        $histories = $query
            ->when($page_size, function ($query) use ($page, $page_size) {
                return $query->offset($page * $page_size)->limit($page_size);
            })
            ->orderBy('date')
            ->get();

        if ($base_currency_id) {
            $base_currency = Currency::find($base_currency_id);
            if (!$base_currency) {
                return response('Cannot find base currency', 404);
            }

            $dates = $query->pluck('date')->toArray();
            $daily_rates = $base_currency->rates()
                ->whereIn('date', $dates)
                ->get();

            $base_currency_rates = [];
            foreach ($daily_rates as $daily_rate) {
                $base_currency_rates[$daily_rate->date] = $daily_rate->rate;
            }

            foreach ($histories as $history) {
                if ($base_currency_rates[$history->date])
                    $history->rate /= $base_currency_rates[$history->date];
                else
                    $history->rate = null;
            }
        }

        return response([
            'currency' => $currency,
            'histories' => $histories,
            'total_count' => $total_count,
        ]);
    }
}

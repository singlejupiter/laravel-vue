<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'char_code' => $this->char_code,
            'nominal' => $this->nominal,
            'name' => $this->name,
            'rate' => $this->rate,
            'max_rate' => $this->rates_max_rate,
            'min_rate' => $this->rates_min_rate,
            'avg_rate' => $this->rates_avg_rate,
        ];
    }
}
